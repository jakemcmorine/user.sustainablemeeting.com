const { promisify } = require('util');
const request = require('request');
const rq = promisify(request);


module.exports.cancelPaypalSubscription = async (subId) => {
    try {
        console.log("In paypal", subId)
        let url = `${CONFIG.paypalUrl}${subId}/cancel`;
        let options = {
            auth: {
                'user': CONFIG.paypalUser,
                'pass': CONFIG.paypalPass
            },
            url:url,
            headers: {
                'Accept': 'application/json',
            },
            method: "POST",
            json: {
                "reason": "Cancelled Via Backend"
            }
          }; 
        let res = await rq(options);
        console.log("Paypal udpated", res);
        console.log("udpated 2", res.status);
        console.log("udpated 2", res.body);
        if(res.body)
          throw new Error("Cancel Subscription Failed");

        return true;
    } catch (error) {
        console.log("$Err", error);
        throw error;
    }
}