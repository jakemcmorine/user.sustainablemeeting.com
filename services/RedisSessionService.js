const validator     = require('validator');
var randomstring    = require("randomstring");
var Redis 			=  require('./../redis/');
var RedisClient 			= require('./../redis/redisModel');
const jwt           	= require('jsonwebtoken');
RedisClient= new RedisClient(Redis);
const createSessionForRegister = async function(userInfo){
   var key = randomstring.generate(4);
    var email =userInfo.email;
    [error,success]= await to(RedisClient.hmset(key,"email",email,"id",userInfo._id.toString(),"status","firststep"));
    if(success){
        [error,success]= await to(RedisClient.lpush(userInfo._id.toString(),key));
         return key;
    }else{
     //   console.log("asdaSDasdASD");
    }
}
module.exports.createSessionForRegister = createSessionForRegister;
 const tokenExpireTime = async function(key,expireInseconds){//returns token
    [error,success]= await to(RedisClient.expire(key,expireInseconds));
    if(success){
       
        return success;
    }else{
        console.log(error);
    }
}

module.exports.tokenExpireTime = tokenExpireTime;
const deleteSession = async function(key){//returns token
   
    [error,success]= await to(RedisClient.del(key));
    if(success){
       return key;
      //console.log(success);
    }else{
      console.log(error);
    }
     
 }
 module.exports.deleteSession = deleteSession;
const createSessionForLogin = async function(userInfo){//returns token
   console.log(userInfo);
   var key = randomstring.generate(4);
   [error,success]= await to(RedisClient.hmset(key,"id",userInfo.id,"email",userInfo.email));
   if(success){
    [error,success]= await to(RedisClient.lpush(userInfo.id,key));
      return key;
     //console.log(success);
   }else{
     console.log(error);
   }
    
}
module.exports.createSessionForLogin = createSessionForLogin;
const deletesessions = async function(id){//returns token
    let userSession = await RedisClient.lrange(id,0,-1);
    if(userSession){
       for(let i=0;i<userSession.length;i++){
           console.log(userSession[i]);
           RedisClient.del(userSession[i]);
       }
       RedisClient.del(id);
       return userSession;
    }else{
          return TE ("tocken expired or removed");
      //  console.log(error);
    }
}
module.exports.deletesessions=deletesessions;
const findSession = async function(key){//returns token
    try{
        let userSession = await RedisClient.hgetall(key);
            return userSession;
      }catch(error){
          throw error;
      }
   
}
module.exports.findSession = findSession;

const extractJWT = async function(key){
    //  let expiration_time = parseInt(CONFIG.jwt_expiration); 
    console.log(CONFIG.jwt_encryption)
      decoded = jwt.verify(key, CONFIG.jwt_encryption);
      return decoded;
    //  return "Bearer "+jwt.sign({user_id:key}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
  };
  module.exports.extractJWT = extractJWT;