// const CONFIG = require('../config/config');
// var request = require('request')
// const jwkToPem = require('jwk-to-pem');
// const jwt = require('jsonwebtoken');
// const AWS = require('aws-sdk');
// global.fetch = require('node-fetch');
// var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
// const { promisify } = require('util');

// AWS.config.update({
//   secretAccessKey: CONFIG.AWS_ACCESSKEY,
//   accessKeyId: CONFIG.AWS_ACCESSKEYID,
// });

// var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({ apiVersion: '2016-04-18', region: "us-east-2" });
// var poolData = {
//   UserPoolId: CONFIG.USER_POOL_ID, // your user pool id here
//   ClientId: CONFIG.APP_CLIENT_ID // your app client id here
// };
// var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);


// class awsObject {
//   //User Sign Up Module
//   async signUpUser(email, password, phoneNumber) {

//     var userData = {
//       Username: 'ucodifyUser', // your username here
//       Pool: userPool
//     };

//     var attributeList = [];

//     var dataEmail = {
//       Name: 'email',
//       Value: email // your email here
//     };
//     var dataPhoneNumber = {
//       Name: 'phone_number',
//       Value: phoneNumber // your phone number here with +country code and no delimiters in front
//     };
//     var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
//     var attributePhoneNumber = new AmazonCognitoIdentity.CognitoUserAttribute(dataPhoneNumber);

//     attributeList.push(attributeEmail);
//     attributeList.push(attributePhoneNumber);

//     var cognitoUser;

//     // resolve and reject both gets returned here.
//     // reject gets handled in controller catch section.
//     return await mySignUp(email, password, attributeList, null);

//   }

//   async userConfirm(email, password, verificationCode) {
//     return await confirmUser(email, password, verificationCode);

//   }

//   async userLogin(userName, password) {
//     //console.log("userName", userName)
//     //console.log("password", password)
//     return await userLoginService(userName, password)

//   }

//   async verifyJwtToken(token) 
//   {
//     try{
//       let jwtValue = await checkJWTToken(token);
//       //console.log("jwtValue",jwtValue);
//       return jwtValue;

//     } catch(err){
//       console.log("err",err)
      
//     }
    
//   }

// }

// const confirmUser = (email, password, verificationCode) => {
//   return new Promise((resolve, reject) => {
//     var params = {
//       ClientId: CONFIG.APP_CLIENT_ID, /* required */
//       ConfirmationCode: verificationCode, /* required */
//       Username: email, /* required */
//       ForceAliasCreation: true || false
//     };
//     cognitoidentityserviceprovider.confirmSignUp(params, function (err, data) {
//       if (err) {
//         console.log(err, err.stack); // an error occurred
//         reject({
//           statusCode: 500,
//           body: JSON.stringify({
//             status: 'FAIL',
//             message: err.message
//           }),
//         });
//       }

//       else {
//         //console.log(data);           // successful response
//         resolve({
//           statusCode: 200,
//           body: JSON.stringify({
//             status: 'SUCCESS',
//             message: '',
//             data: {
//               // username: cognitoUser.getUsername(),
//               // id: result.userSub
//               data: data
//             }
//           }),
//         });
//       }
//     });

//   });
// }

// const mySignUp = (email, password, attributes, someparam) => {
//   return new Promise((resolve, reject) => {
//     userPool.signUp(email, password, attributes, someparam, function (err, result) {

//       let data = {};

//       if (err) {
//         reject({
//           statusCode: 500,
//           body: JSON.stringify({
//             status: 'FAIL',
//             message: err.message
//           }),
//         });
//       } else {
//         let cognitoUser = result.user;

//         resolve({
//           statusCode: 200,
//           body: JSON.stringify({
//             status: 'SUCCESS',
//             message: '',
//             data: {
//               username: cognitoUser.getUsername(),
//               id: result.userSub
//             }
//           }),
//         });
//       }
//     })
//   });
// }

// const checkJWTToken = (token) => {
//   return new Promise((resolve, reject) => {

//     let pool_region = CONFIG.AWS_REGION;
//     let UserPoolId = CONFIG.USER_POOL_ID;
//     request({
//       url: `https://cognito-idp.${pool_region}.amazonaws.com/${UserPoolId}/.well-known/jwks.json`,
//       json: true
//     }, function (error, response, body) {
//       //console.log(response)
//       //console.log("body",body)
//       if (!error && response.statusCode === 200) {
//         pems = {};
//         var keys = body['keys'];
//         for (var i = 0; i < keys.length; i++) {
//           //Convert each key to PEM
//           var key_id = keys[i].kid;
//           var modulus = keys[i].n;
//           var exponent = keys[i].e;
//           var key_type = keys[i].kty;
//           var jwk = { kty: key_type, n: modulus, e: exponent };
//           var pem = jwkToPem(jwk);
//           pems[key_id] = pem;
//         }
//         //validate the token
//         var decodedJwt = jwt.decode(token, { complete: true });
//         //console.log("decodedJwt",decodedJwt)
//         if (decodedJwt ==null) {
//           console.log("Not a valid JWT token");
//           reject({
//             statusCode: 500,
//             body: JSON.stringify({
//               status: 'FAIL',
//               message: "Jwt Token Not Valid"
//             }),
//           });
//           //return false;
//         }

//         var kid = decodedJwt.header.kid;
//         var pem = pems[kid];
//         if (!pem) {
//           //console.log('Invalid token');
//           reject({
//             statusCode: 500,
//             body: JSON.stringify({
//               status: 'FAIL',
//               message: "Jwt Token Not Invalid"
//             }),
//           });
//         }

//         jwt.verify(token, pem, function (err, payload) {
//           if (err) {
//             //console.log("Invalid Token.");
//             //console.log(err)
//             reject({
//               statusCode: 500,
//               body: JSON.stringify({
//                 status: 'FAIL',
//                 message: "Jwt Token Not Invalid"
//               }),
//             });
//           } else {
//             //console.log("Valid Token.");
//             //console.log(payload);
//             resolve({
//               statusCode: 200,
//               body: JSON.stringify({
//                 status: 'SUCCESS',
//                 message: 'Jwt token Valid',
//                 data: {
//                   payload: payload
//                 }
//               }),
//             });
//           }
//         });
//       } else {
//         //console.log(error)
//         //console.log("Error! Unable to download JWKs");
//       }
//     });


//   })
// }

// const userLoginService = (userName, password) => {
//   return new Promise((resolve, reject) => {
//     var poolData = {
//       UserPoolId: CONFIG.USER_POOL_ID, // your user pool id here
//       ClientId: CONFIG.APP_CLIENT_ID // your app client id here
//     };
//     var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
//     var userData = {
//       Username: userName, // your username here
//       Pool: userPool
//     };
//     var authenticationData = {
//       Username: userName, // your username here
//       Password: password, // your password here
//     };
//     var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);

//     var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
//     cognitoUser.authenticateUser(authenticationDetails, {
//       onSuccess: function (result) {
//         var accessToken = result.getAccessToken().getJwtToken();
//         //console.log(accessToken)
//         resolve({
//           statusCode: 200,
//           body: JSON.stringify({
//             status: 'SUCCESS',
//             message: '',
//             data: {
//               username: cognitoUser.getUsername(),
//               id: result.userSub,
//               accessToken: accessToken
//             }
//           }),
//         });
//       },
//       onFailure: function (err) {
//         //console.log(err);
//         reject({
//           statusCode: 500,
//           body: JSON.stringify({
//             status: 'FAIL',
//             message: err.message
//           }),
//         });
//       },
//       mfaRequired: function (codeDeliveryDetails) {
//         var verificationCode = prompt('Please input verification code', '');
//         cognitoUser.sendMFACode(verificationCode, this);
//       }
//     });
//   });
// }

// module.exports = new awsObject();