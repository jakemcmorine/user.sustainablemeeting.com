const awsservices = require('./awsServices');
module.exports.checkjwtToken = async(req,res,next)=>{
    //console.log(req)
    if(req.cookies.jwtToken)
    {
        let jwt_token =req.cookies.jwtToken;
        let validJwtToken =  await awsservices.verifyJwtToken(jwt_token);
        //let parsedTokenData = JSON.parse(validJwtToken);
        //console.log("validJwtToken",validJwtToken);
        if(validJwtToken.statusCode == 200 )
        {
            next();
        }
        else{
            res.redirect('/')
        }
        
    }
    else{
        res.redirect('/')
        //next()
    }
};