jQuery(document).ready(function() {
    jQuery('#example').DataTable({
        columnDefs  : [{ orderable: false, targets: [4] }],
        aaSorting   : [[0,'asc']]
    });

    jQuery("#alertBlock").hide();

    jQuery('#example').on('click','.manageRights', function() {
        var data_id     = jQuery(this).attr('data-id');
        window.location.href = `/admin/manage-rights/${data_id}`;
    });

    jQuery('#example').on('click','.resendRow', function() {
        var data_id     = jQuery(this).attr('data-id');
        var data_code   = jQuery(this).attr('data-code').split('|');
        var firstName   = data_code[0];
        var email       = data_code[1];

        jQuery.confirm({
            title: 'Confirmation',
            content: `Are you sure you want to resend the user confirm email to ${firstName}`,
            type: 'green',
            buttons: {
                ok: {
                    text: "Ok",
                    btnClass: 'btn-success',
                    keys: ['enter'],
                    action: function(){
                        openOverlay();
                        jQuery.post('/resendConfirmEmail', {email:email, adminId:data_id}, function(data) {
                            if(data.success === 1) {
                                closeOverlay();
                                jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                                jQuery("#alertType").text("Success");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                setTimeout(function() {
                                    window.location.href = '/admin/list';
                                }, 5000);
                            }else {
                                closeOverlay();
                                jQuery("#alertBlock").removeClass('alert-success').addClass("alert-danger");
                                jQuery("#alertType").text("Failed");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                setTimeout(function() {
                                    window.location.href = '/admin/list';
                                }, 5000);
                            }
                        });    
                    }
                },
                cancel: function(){
                    return true;
                }
            }
        });
    });

    jQuery('#example').on('click','.blockRow', function() {
        var data_id     = jQuery(this).attr('data-id');
        var data_code   = jQuery(this).attr('data-code').split('|');
        var firstName   = data_code[0];
        var isBlocked   = data_code[1];
        var email       = data_code[2];
        var act         = isBlocked == 1 ?'block':'un-block';

        jQuery.confirm({
            title: 'Confirmation',
            content: `Are you sure you want to ${act}: ${firstName}`,
            type: 'blue',
            buttons: {
                ok: {
                    text: "Ok",
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: function(){
                        openOverlay();
                        jQuery.post('/admin/manageBlockAdmin', {email:email, adminId:data_id, isBlocked:isBlocked}, function(data) {
                            if(data.success === 1) {
                                closeOverlay();
                                jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                                jQuery("#alertType").text("Success");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                setTimeout(function() {
                                    window.location.href = '/admin/list';
                                }, 5000);
                            }else {
                                jQuery("#alertBlock").removeClass('alert-success').addClass("alert-danger");
                                jQuery("#alertType").text("Failed");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                setTimeout(function() {
                                    window.location.href = '/admin/list';
                                }, 5000);
                            }
                        });    
                    }
                },
                cancel: function(){
                    return true;
                }
            }
        });
    });
});