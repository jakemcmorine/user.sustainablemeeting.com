jQuery(document).ready(function() {
    jQuery("#alertBlock").hide();

    jQuery("#btnReset").click(function() {
        window.location.reload(true);
    });

    jQuery("#btnBack").click(function() {
        window.history.back();
    });

    jQuery('#frmEdit').validate({
        rules: {
            firstName: {
                required:true,
                maxlength:50
            },
            lastName: {
                required:true,
                maxlength:50
            }
        },
        messages: {
            firstName: {
                required: "Please Enter First Name"
            },
            lastName: {
                required: "Please Enter Last Name"
            }
        },
        submitHandler: function() {
            jQuery('#btnBlock').hide();
            jQuery('#email').val(emailField);
            jQuery('#adminId').val(adminIdField);
            var formData = jQuery('#frmEdit').serializeArray();
            jQuery.post('/admin/editAdmin', formData, function(data) {
                if(data.success === 1){
                    jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                    jQuery("#alertType").text("Success");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                    setTimeout(function() {
                        window.location.href = '/admin/list';
                    }, 5000);
                }else {
                    jQuery('#btnBlock').show();
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                }
            });
        }
    });

    jQuery('#btnDelete').click(function() {
        jQuery.confirm({
            title: 'Confirmation',
            content: 'Are you sure you want to permanently delete?',
            type: 'red',
            buttons: {   
                ok: {
                    text: "Ok",
                    btnClass: 'btn-danger',
                    keys: ['enter'],
                    action: function() {
                        jQuery('#btnBlock').hide();
                        openOverlay();
                        jQuery.post('/admin/deleteAdmin', {email:emailField, adminId:adminIdField}, function(data) {
                            if(data.success === 1) {
                                closeOverlay();
                                jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                                jQuery("#alertType").text("Success");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                setTimeout(function() {
                                    window.location.href = '/admin/list';
                                }, 3000);
                            }else {
                                closeOverlay();
                                jQuery("#alertBlock").removeClass('alert-success').addClass("alert-danger");
                                jQuery("#alertType").text("Failed");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                jQuery('#btnBlock').show();
                            }
                        });
                    }
                },
                cancel: function(){
                    return true;
                }
            }
        });
    });
});