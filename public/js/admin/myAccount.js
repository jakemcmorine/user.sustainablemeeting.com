jQuery(document).ready(function() {
    jQuery("#alertBlock").hide();

    jQuery("#btnReset").click(function() {
        window.location.reload(true);
    });

    jQuery("#btnBack").click(function() {
        window.history.back();
    });

    jQuery('#btnUploadPhoto').click(function() {
        changePhoto();
    });

    jQuery('#frmMyAccount').validate({
        rules: {
            firstName: {
                required:true,
                maxlength:50
            },
            lastName: {
                required:true,
                maxlength:50
            }
        },
        messages: {
            firstName: {
                required: "Please Enter First Name"
            },
            lastName: {
                required: "Please Enter Last Name"
            }
        },
        submitHandler: function() {
            jQuery('#btnBlock').hide();
            var formData = jQuery('#frmMyAccount').serializeArray();
            jQuery.post('/admin/myAccount', formData, function(data) {
                if(data.success === 1){
                    jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                    jQuery("#alertType").text("Success");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                    setTimeout(function() {
                        window.location.reload(true);
                    }, 3000);
                }else {
                    jQuery('#btnBlock').show();
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                }
            });
        }
    });
});

var changePhoto = function() {
    var files = jQuery('#adminPhoto').get(0).files;
    if(files.length > 0) {
        var fileType = files[0].type;
        if(fileType.includes('image')) {
            var formData = new FormData();
            formData.append('adminPhoto', files[0]);
            jQuery.ajax({
                url: "/admin/uploadAdminPhoto"
                ,type:'post'
                ,data:formData
                ,processData:false
                ,contentType:false
                ,beforeSend: function() {
                    jQuery('#alertBlock').hide();
                    openOverlay();
                }
                ,success: function(data) {
                    if(data.success === 1) {
                        jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                        jQuery("#alertType").text("Success");
                        jQuery("#alertMsg").text(data.message);
                        jQuery('#alertBlock').show();
                        setTimeout(function() {
                            window.location.reload(true);
                        }, 3000);
                    }else {
                        jQuery("#alertBlock").addClass("alert-danger");
                        jQuery("#alertType").text("Failed");
                        jQuery("#alertMsg").text(data.message);
                        jQuery('#alertBlock').show();
                    }
                }
                ,error: function(data){
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text('An unknown error found, please try later.');
                    jQuery('#alertBlock').show();
                }
                ,complete: function(){
                    closeOverlay();
                }
            });
        }else {
            jQuery("#alertBlock").addClass("alert-danger");
            jQuery("#alertType").text("Failed");
            jQuery("#alertMsg").text('The selected file type should be image.');
            jQuery('#alertBlock').show();
        }
    }else {
        // jQuery("#alertBlock").addClass("alert-danger");
        // jQuery("#alertType").text("Failed");
        // jQuery("#alertMsg").text('Please select your image.');
        // jQuery('#alertBlock').show();
    }
}