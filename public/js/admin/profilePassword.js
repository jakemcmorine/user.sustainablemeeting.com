jQuery(document).ready(function() {
    jQuery.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
    });

    jQuery('#btnReset').click(function() {
        window.location.reload(true);
    });

    jQuery("#btnBack").click(function() {
        window.history.back();
    });

    jQuery('#frmProfilePassword').validate({
        rules: {
            password: {
                required:true,
                minlength:6,
                maxlength:12,
                pwcheck:true
            },
            newPassword: {
                required:true,
                minlength:6,
                maxlength:12,
                pwcheck:true
            },
            confirmPassword: {
                required:true,
                minlength:6,
                maxlength:12,
                equalTo: "#newPassword"
            }
        },
        messages: {
            password: {
                required: "Please Enter Current Password",
                pwcheck: "Password must includes alphabets and numbers"
            },
            newPassword: {
                required: "Please Enter New Password",
                pwcheck: "Password must includes alphabets and numbers"
            },
            confirmPassword: {
                required: "Please Confirm Your Password",
                equalTo:"The New Password and Confirm Password should be match"
            }
        },
        submitHandler: function() {
            jQuery('#btnBlock').hide();
            openOverlay();
            var formData = jQuery('#frmProfilePassword').serializeArray();
            jQuery.post('/admin/profilePassword', formData, function(data) {
                if(data.success === 1){
                    jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                    jQuery("#alertType").text("Success");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                    setTimeout(function() {
                        window.location.href = '/doLogout';
                    }, 5000);
                }else {
                    jQuery('input[type="password"]').val('');
                    jQuery('#btnBlock').show();
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                }
                closeOverlay();
            });
        }
    });
});