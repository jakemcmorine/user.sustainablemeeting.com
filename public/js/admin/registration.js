jQuery(document).ready(function() {
    jQuery("#alertBlock").hide();

    jQuery("#btnReset").click(function() {
        window.location.reload(true);
    });

    jQuery('#frmAdd').validate({
        rules: {
            firstName: {
                required:true,
                maxlength:50
            },
            lastName: {
                required:true,
                maxlength:50
            },
            email: {
                required:true,
                email:true
            }
        },
        messages: {
            firstName: {
                required: "Please Enter First Name"
            },
            lastName: {
                required: "Please Enter Last Name"
            },
            email: {
                required: "Please Enter E-mail"
            }
        },
        submitHandler: function() {
            openOverlay();
            jQuery('#btnBlock').hide();
            var formData = jQuery('#frmAdd').serializeArray();
            jQuery.post('/admin/registration', formData, function(data) {
                if(data.success === 1){
                    jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                    jQuery("#alertType").text("Success");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                    setTimeout(function() {
                        window.location.href = '/admin/list';
                    }, 5000);
                }else {
                    jQuery('#btnBlock').show();
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                }
                closeOverlay();
            });
        }
    });
});