$(document).ready(function () {
    $('#example').DataTable({
        columnDefs: [{ orderable: false, targets: [0, 2, 3] }, { searchable: false, targets: [0, 3] }],
        aaSorting: [[1, 'asc']]
    });
    let updateMessage = "/report/update-message"
    $('.sent').on("click", function () {
        let id = $(this).data("id");
        $("#report_id").text(id);
    });
    $(".confirm").click(function () {
        let id = $("#report_id").text();
        let message = $("#message").val();
        $(".message_" + id).text(message);
        let type = "message";
        axios.post(updateMessage, { id, message, type }
        ).then(response => {
            console.log(response)
        });
        $("#message").val('');
    });
    $(".status").on("change", function () {
        let id = $(this).data("id");
        let status = $(this).val();
        let type = "status";
        axios.post(updateMessage, { id, status, type }
        ).then(response => {
            console.log(response)
        });

    });
});
