jQuery(document).ready(function() {
    jQuery("#alertBlock").hide();

    jQuery("#btnReset").click(function() {
        window.location.reload(true);
    });

    jQuery("#btnBack").click(function() {
        window.history.back();
    });

    jQuery('#btnUpload').click(function() {
        changePhoto();
    });

    jQuery('#frmEdit').validate({
        rules: {
            name: {
                required:true,
                maxlength:50
            }
        },
        messages: {
            name: {
                required: "Please Enter Name"
            }
        },
        submitHandler: function() {
            jQuery('#btnBlock').hide();
            var formData = jQuery('#frmEdit').serializeArray();
            var url = `/b2b/list/${b2bId}`;
            jQuery.post(url, formData, function(data) {
                if(data.success === 1){
                    jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                    jQuery("#alertType").text("Success");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                    setTimeout(function() {
                        window.location.href = '/b2b/list';
                    }, 3000);
                }else {console.log(data);
                    jQuery('#btnBlock').show();
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text(data.message);
                    jQuery('#alertBlock').show();
                }
            });
        }
    });

    jQuery('#btnDelete').click(function() {
        jQuery.confirm({
            title: 'Confirmation',
            content: 'Are you sure you want to permanently delete?',
            type: 'red',
            buttons: {   
                ok: {
                    text: "Ok",
                    btnClass: 'btn-danger',
                    keys: ['enter'],
                    action: function(){
                        jQuery.ajax({
                            url: `/b2b/deleteProfile/${b2bId}`
                            ,type:'post'
                            ,data:{}
                            ,processData:false
                            ,contentType:false
                            ,beforeSend: function() {
                                jQuery('#alertBlock').hide();
                                jQuery('#btnBlock').hide();
                                openOverlay();
                            }
                            ,success: function(data) {
                                if(data.success === 1) {
                                    jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                                    jQuery("#alertType").text("Success");
                                    jQuery("#alertMsg").text(data.message);
                                    jQuery('#alertBlock').show();
                                    setTimeout(function() {
                                        window.location.href = '/b2b/list';
                                    }, 2000);
                                }else {
                                    jQuery("#alertBlock").addClass("alert-danger");
                                    jQuery("#alertType").text("Failed");
                                    jQuery("#alertMsg").text(data.message);
                                    jQuery('#alertBlock').show();
                                    jQuery('#btnBlock').show();
                                }
                            }
                            ,error: function(data){
                                jQuery("#alertBlock").addClass("alert-danger");
                                jQuery("#alertType").text("Failed");
                                jQuery("#alertMsg").text('An unknown error found, please try later.');
                                jQuery('#alertBlock').show();
                            }
                            ,complete: function(){
                                closeOverlay();
                            }
                        });
                    }
                },
                cancel: function(){
                    return true;
                }
            }
        });
    });
});

var changePhoto = function() {
    var files = jQuery('#logo').get(0).files;
    if(files.length > 0) {
        var fileType = files[0].type;
        if(fileType.includes('image')) {
            var formData = new FormData();
            formData.append('logo', files[0]);
            jQuery.ajax({
                url: `/b2b/uploadLogo/${b2bId}`
                ,type:'post'
                ,data:formData
                ,processData:false
                ,contentType:false
                ,beforeSend: function() {
                    jQuery('#alertBlock').hide();
                    openOverlay();
                }
                ,success: function(data) {
                    if(data.success === 1) {
                        jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                        jQuery("#alertType").text("Success");
                        jQuery("#alertMsg").text(data.message);
                        jQuery('#alertBlock').show();
                        setTimeout(function() {
                            window.location.reload(true);
                        }, 3000);
                    }else {
                        jQuery("#alertBlock").addClass("alert-danger");
                        jQuery("#alertType").text("Failed");
                        jQuery("#alertMsg").text(data.message);
                        jQuery('#alertBlock').show();
                    }
                }
                ,error: function(data){
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text('An unknown error found, please try later.');
                    jQuery('#alertBlock').show();
                }
                ,complete: function(){
                    closeOverlay();
                }
            });
        }else {
            jQuery("#alertBlock").addClass("alert-danger");
            jQuery("#alertType").text("Failed");
            jQuery("#alertMsg").text('The selected file type should be image.');
            jQuery('#alertBlock').show();
        }
    }else {
        // jQuery("#alertBlock").addClass("alert-danger");
        // jQuery("#alertType").text("Failed");
        // jQuery("#alertMsg").text('Please select your image.');
        // jQuery('#alertBlock').show();
    }
}