jQuery(document).ready(function() {
    jQuery("#alertBlock").hide();

    jQuery("#btnReset").click(function() {
        window.location.reload(true);
    });

    jQuery("#btnBack").click(function() {
        window.history.back();
    });

    jQuery('#frmAdd').validate({
        rules: {
            name: {
                required:true,
                maxlength:50
            },
            logo: {
                required:true
            }
        },
        messages: {
            name: {
                required: "Please Enter Name"
            },
            logo: {
                required: "Please Select Logo"
            }
        },
        submitHandler: function() {
            jQuery('#btnBlock').hide();
            var formData = new FormData();
            var name = jQuery('#name').val();
            var files = jQuery('#logo').get(0).files;

            formData.append('logo', files[0]);
            formData.append('name', name);

            jQuery.ajax({
                url: "/b2b/registration"
                ,type:'post'
                ,data:formData
                ,processData:false
                ,contentType:false
                ,beforeSend: function() {
                    jQuery('#alertBlock').hide();
                    openOverlay();
                }
                ,success: function(data) {
                    if(data.success === 1) {
                        jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                        jQuery("#alertType").text("Success");
                        jQuery("#alertMsg").text(data.message);
                        jQuery('#alertBlock').show();
                        setTimeout(function() {
                            window.location.href = '/b2b/list';
                        }, 3000);
                    }else {
                        jQuery("#alertBlock").addClass("alert-danger");
                        jQuery("#alertType").text("Failed");
                        jQuery("#alertMsg").text(data.message);
                        jQuery('#alertBlock').show();
                        jQuery('#btnBlock').show();
                    }
                }
                ,error: function(data){
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#alertType").text("Failed");
                    jQuery("#alertMsg").text('An unknown error found, please try later.');
                    jQuery('#alertBlock').show();
                }
                ,complete: function(){
                    closeOverlay();
                }
            });
        }
    });
});