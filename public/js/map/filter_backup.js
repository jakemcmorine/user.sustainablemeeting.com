var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

var grayscale = L.tileLayer(mbUrl, { id: 'mapbox.light', attribution: mbAttr }),
    streets = L.tileLayer(mbUrl, { id: 'mapbox.streets', attribution: mbAttr });

var map = L.map('map-canvas', {
    center: [41.92, -70.34],
    zoom: 8,
    layers: [grayscale]
});
var baseLayers = {
    "Grayscale": grayscale,
    "Streets": streets
};

L.control.layers(baseLayers).addTo(map);
let layerId;
let query = {};
getBounds();

$("#search-data").keydown(function () {
    if ($(this).val().length > 1) {
        getBounds();
    }
});
$(".search-feilds").on("change", () => {
    getBounds();
})
map.on('dragend zoomend', function () {
    getBounds();
});
function getBounds() {
    const bound = map.getBounds();

    console.log(bound)
    console.log(bound._southWest.lat)
    console.log(bound._northEast.lat)
    query = {
        "query": {
            "bool": {
                /* "filter": {
                    "geo_bounding_box": {
                        "placement": {
                            "locations": {
                                "top_right": { "lat": bound._northEast.lat, "lon": bound._northEast.lng },
                                "bottom_left": { "lat": bound._southWest.lat, "lon": bound._southWest.lng }
                            }
                        }
                    }
                } */
            }
        }
    };
    filterMarkers();
}
function filterMarkers() {

    const search = $("#search-data").val().toLowerCase();
    const rarity = $("#rarity").val();
    const type = $("#type").val();
    const subspace = $("#subspace").val();
    let should = {};
    let must = [];
    if (search != '' && search != undefined) {
        should = {
            "wildcard": {
                "placementNAme": `${search}*`
            }
        };
    }
    if (rarity != '' && rarity != undefined) {
        temp = {
            "match": {
                "rarity": `${rarity}`
            }
        }
        must.push(temp)

    }
    if (type != '' && type != undefined) {
        temp = {
            "match": {
                "objectType": `${type}`
            }
        }
        must.push(temp)

    }
    if (subspace != '' && subspace != undefined) {
        temp = {
            "match": {
                "subsapce": `${subspace}`
            }
        }
        must.push(temp)

    }
    if (must != undefined)
        query["query"]["bool"]["must"] = must;

    if (Object.keys(should).length > 0) {
        query["query"]["bool"]["should"] = should;
        query["query"]["bool"]["minimum_should_match"] = 1;
    }
    //alert(query)
    getData();
}

function getData() {
    // event.preventDefault();
    const baseUrl = "https://search-nnc-5q44xz65yaffamkljy7piz42ga.ca-central-1.es.amazonaws.com/"
    $.ajax({
        url: baseUrl + "artist/object/_search?filter_path=hits",
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify(query),
        success: (data) => {
            //alert(data);
            //console.log(data);
            setMarkers(data.hits);
        },
        error: (jqXHR, textStatus, errorThrown) => {
            console.log(textStatus);
            console.log(errorThrown);
        }

    });
}

function setMarkers(data) {
    //map.removeLayer(map)

    if (layerId) {

        map.removeLayer(layerId);
        console.log("rem");
    }
    var markers = L.layerGroup();
    data.hits.forEach((obj) => {
        console.log(obj);
        console.log('ID: ' + obj._source.id);
        console.log('Name: ' + obj._source.placementNAme);
        console.log('Type: ' + obj._source.objectType);
        console.log('Rarity: ' + obj._source.rarity);
        console.log('Subsapce: ' + obj._source.subsapce);
        console.log('Methord: ' + obj._source.placementMethord);
        let popup = `Name : ${obj._source.placementNAme} </br>
                Type : ${obj._source.objectType} </br>`
        if (obj._source.locations !== undefined)
            obj._source.locations.forEach((loc) => {
                /*   var myIcon = L.divIcon({className: 'leaflet-div-icon'});
                  L.marker([loc.lat, loc.lon], {icon: myIcon}).bindTooltip(popup).addTo(markers); */
                L.marker([loc.lat, loc.lon]).bindTooltip(popup).addTo(markers);
                console.log("Marker")

            })

    });

    layerId = markers.addTo(map);
}