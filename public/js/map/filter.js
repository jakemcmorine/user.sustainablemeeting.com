var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

var grayscale = L.tileLayer(mbUrl, { id: 'mapbox.light', attribution: mbAttr }),
    streets = L.tileLayer(mbUrl, { id: 'mapbox.streets', attribution: mbAttr }),
    center = [50.00, 20.00];

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
        var latlng = L.latLng(position.coords.latitude, position.coords.longitude);
        map.flyTo(latlng, 5);
        map.once('moveend', function () {
            filterMarkers();
        });
    });
} else {
    filterMarkers();
    alert("Geolocation is not supported by this browser.");
}
var map = new L.map('map-canvas', {
    center: center,
    zoom: 5,
    minZoom: 5,
    maxZoom: 15,
    layers: [grayscale]
});

var baseLayers = {
    "Grayscale": grayscale,
    "Streets": streets
};


L.control.layers(baseLayers).addTo(map);

map.addControl(new L.Control.Fullscreen());

let query = {};
var markersList = [];
let markers = new L.MarkerClusterGroup({ showCoverageOnHover: false });

$("#search-data").keyup(function () {
    getSuggestion($(this).val(), 'objectSearch');
});

$("#placementName").keyup(function () {
    getSuggestion($(this).val(), 'placementSearch');
});

$(".search-feilds").on("change", () => {
    filterMarkers();
})
map.on('dragend zoomend', function () {  // zoomend
    filterMarkers();
});
function filterMarkers() {

    console.log("filter Started")
    const rarity = $("#rarity").val();
    const type = $("#type").val();
    const PlacementType = $("#PlacementType").val();
    const subspace = $("#subspace").val();
    const bound = map.getBounds();
    let match = {};
    let should = {};
    let must = [];
    let childMust = [];

    if (subspace != '' && subspace != undefined) {
        match = {
            "match": {
                "subspace": `${subspace}`
            }
        }
        childMust.push(match)
    }
    if (type != '' && type != undefined) {
        temp = {
            "match": {
                "objectType": `${type}`
            }
        }
        childMust.push(temp)

    }
    if (PlacementType != '' && PlacementType != undefined) {
        match = {
            "match": {
                "placementType": `${PlacementType}`
            }
        }
        childMust.push(match)
    }


    if (bound != '' && bound != undefined) {

        temp = {
            "geo_bounding_box": {
                "location": {
                    "top_right": { "lat": bound._northEast.lat, "lon": bound._northEast.lng },
                    "bottom_left": { "lat": bound._southWest.lat, "lon": bound._southWest.lng }
                }
            }
        }
        childMust.push(temp)

        temp = {
            "has_child": {
                "type": "placement",
                "query": {
                    "bool": {
                        "must": childMust
                    }
                },
                "inner_hits": { "size": 100000 }
            }
        }

        must.push(temp)
    }
    /* 
        if (search != '' && search != undefined) {
            should = {
                "wildcard": {
                    "objName": `${search}*`
                }
            };
        } */
    if (rarity != '' && rarity != undefined) {
        temp = {
            "match": {
                "rarity": `${rarity}`
            }
        }
        must.push(temp)

    }



    query = {
        "query": {
            "bool": {
            }
        }
    };
    if (must != undefined)
        query["query"]["bool"]["must"] = must;

    if (Object.keys(should).length > 0) {
        query["query"]["bool"]["should"] = should;
        query["query"]["bool"]["minimum_should_match"] = 1;
    }
    getData();
}

function getData() {
    let zoom = map.getZoom();
    let aggs = {
        "placement": {
            "children": {
                "type": "placement"
            },
            "aggs": {
                "cells": {
                    "geohash_grid": {
                        "field": "location",
                        "precision": 1
                    },
                    "aggs": {
                        "center_lat": {
                            "avg": {
                                "script": "doc['location'].lat"
                            }
                        },
                        "center_lon": {
                            "avg": {
                                "script": "doc['location'].lon"
                            }
                        }
                    }
                }
            }
        }
    };
    if (zoom  < 11) {
        query["aggs"] = aggs;
        query["size"] = 0;
    }
    console.log("zz", zoom)
    const admin_url = "/map/get-newmap-placements";
    axios({
        method: 'post',
        url: admin_url,
        data: query
    }).then(response => {
        console.log(response.data)
        var data = response.data.data;
        //console.log(data.hits.length, data.hits)
        if (zoom  >= 11 && data.hits != undefined)
            setMarkers(data.hits);
        if(zoom  < 11 &&  data.aggregations !== undefined)
            setMarkersClasters(data.aggregations);
    }
    );
}
function getSuggestion(prefix, type) {
    const baseUrl = "/map/get-search-suggetion"
    const suggest = {
        "suggest": {
            "Objname": {
                "prefix": prefix,
                "completion": {
                    "field": "objName",
                    //"skip_duplicates": true
                }
            },
            "PlacementName": {
                "prefix": prefix,
                "completion": {
                    "field": "placementName",  // update after Abin added the Completion mapping with PlacementName
                    "skip_duplicates": true
                }
            }
        }
    }

    axios({
        method: 'post',
        url: baseUrl,
        data: suggest
    }).then(response => {

        var data = response.data.data;

        let html = '';
        if (type == 'objectSearch') {
            data.suggest.Objname[0].options.forEach(suggestion => {
                html += `<li >${suggestion.text}</li>`
            });
            if (html != '') {
                $("#suggestions").show();
            } else {
                $("#suggestions").hide();
            }
            $("#suggestions").html(html);
        }
        else {
            data.suggest.PlacementName[0].options.forEach(suggestion => {
                // console.log("loc", suggestion._source.location)
                let loc = suggestion._source.location;
                html += `<li class="navigation"  data-lat="${loc.lat}" data-lon="${loc.lon}" data-name=>${suggestion.text}</li>`
            });
            if (html != '') {
                $("#placement-suggestions").show();
            } else {
                $("#placement-suggestions").hide();
            }
            $("#placement-suggestions").html(html);

        }
    }
    );
}

function setMarkers(data) {
    markersList = [];
    markers.clearLayers();

    data.hits.forEach((obj) => {
        // console.log(obj);
        let popup = `Name : ${obj._source.objName}</br>
                Type : ${obj._source.objectType}</br>
                Rarity : ${obj._source.rarity}</br>`
        let colour = `${obj._source.rarity}-icon`;


        placements = obj.inner_hits.placement.hits.hits;
        placements.forEach((placement) => {
            if (obj._source.rarity === undefined) {
                popup = `Name : ${placement._source.placementName}</br>
                Type : Treasure Chest</br>
                Rarity : ${placement._source.rarity}</br>`
                colour = `${placement._source.rarity}-icon`;
            }
            pinPoint = placement._source.location;
            if (pinPoint !== undefined && pinPoint !== null) {
                //L.marker([pinPoint.lat, pinPoint.lon]).bindTooltip(popup).addTo(markers);
                var myIcon = L.divIcon({ className: colour });
                var m = L.marker([pinPoint.lat, pinPoint.lon], { icon: myIcon }).bindTooltip(popup).on('click', markerOnClick);  //.addTo(markers);     
                // var m = L.marker([pinPoint.lat, pinPoint.lon]) //.addTo(markers);     

                markersList.push(m);
                markers.addLayer(m);
            }
        });
    });
    map.addLayer(markers);
}
function setMarkersClasters(data) {
    markersList = [];
    markers.clearLayers();

    data.placement.cells.buckets.forEach((obj) => {

        console.log("obj", obj);
        //L.marker([pinPoint.lat, pinPoint.lon]).bindTooltip(popup).addTo(markers);
        var myIcon = L.divIcon({ className: 'Green-icon' });
        let count =  obj.doc_count;
        var m = L.marker([obj.center_lat.value, obj.center_lon.value],{icon:myIcon, opacity: 0.5 }).bindTooltip(count.toString(), {permanent: true, className: "my-label", offset: [0, 0] }).addTo(map);  //.addTo(markers);     
        // var m = L.marker([pinPoint.lat, pinPoint.lon]) //.addTo(markers);     
        
        markersList.push(m);
        markers.addLayer(m);
    });
    //markers = markersList.addLayer(markers);
     map.addLayer(markers);
    //var marker = L.marker([9.939093, 76.270523]).addTo(map);

}

function navigatePlacement(loc) {
    alert(loc)
}
function markerOnClick(e) {
    var r = confirm("Are you sure to navigate to Object List");
    if (r == true) {
        window.location = "/object/list";
    }
}

$(document).ready(() => {
    $(".suggestions-container").on("click", '.navigation', function () {
        $("#placement-suggestions").hide();
        $("#placementName").val('');
        var lat = $(this).data('lat');
        var lon = $(this).data('lon');
        var latlng = L.latLng([lat, lon]);
        map.flyTo(latlng, 15);
    })
});
