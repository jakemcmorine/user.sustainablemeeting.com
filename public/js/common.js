jQuery(document).ready(function() {
    var aURL = window.location.href.split('/');
    if(aURL.length>0) {
        let sChild = aURL[aURL.length-1];
        let objChild  = jQuery('#'+sChild);

        if(jQuery.type(objChild) != "undefined") {
            let sParent = objChild.attr('data-group-link');
            if(jQuery.type(sParent) != "undefined") {
                //jQuery('#'+sParent).trigger('click');
                objChild.parent().addClass('active');
            }
        }
    }
});
$(function () {
    setNavigation();
});

function setNavigation() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);
    $(".navbar-sidebar a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            $(this).closest('li').addClass('active');
             $(this).closest('.has-sub').addClass('active');
            // $(this).closest('.has-sub').find('.js-arrow').addClass('open');
            $(this).closest('.has-sub').find('.js-arrow').trigger('click');
           
        }
       
    });
    
}


var openOverlay = function() {
    jQuery("#overlay").show();
    jQuery("#overlayNav").show();
}

var closeOverlay = function() {
    jQuery("#overlay").hide();
    jQuery("#overlayNav").hide();
}

var manageFocusMenu = function(parentMenuKey) {
    var objChild  = jQuery('#'+parentMenuKey);
    if(jQuery.type(objChild) != "undefined") {
        var sParent = objChild.attr('data-group-link');
        if(jQuery.type(sParent) != "undefined") {
            jQuery('#'+sParent).trigger('click');
            objChild.parent().addClass('active');
        }
    }
}

