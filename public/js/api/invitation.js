Vue.use(VeeValidate);
const app = new Vue({
    el: '#invitation',
    data: {
      datas: [],
      error:false,
     email:"",
      id:"",
      success:false,
    },
    
   
    methods: {
      getAll() {
       
      },
      deleteUSer(id){
          this.id=id;
      },
      ajaxSetup(){
          $.ajaxSetup({
            beforeSend: function (xhr)
            {
               xhr.setRequestHeader("Authorization",document.cookie);        
            }
        });
      },
      handleSubmit(e) {
          //alert(e)
        this.submitted = true;
        this.$validator.validateAll("step1").then(valid => {
            if (valid) {
               this.sentInvitation({"email":this.email})
            }else{
                console.log(this.$validator)
               // alert("false")
            }
        });
    },


  
      shoLoader(){
         $(".loader").css({"display":"none"});
         var frm = document.getElementsByName('add-manually')[0];
         $("#scss").text("user deleted successfully")
         $("#error").hide();
      },
      hideLoader(){   
          $(".loader").css({"display":"none"});
          $("#success").hide();
          $("#error").show();
      },
      sentInvitation(data){
       this.ajaxSetup();
      $.post("/sentInvitation",
                  data,{withCredentials: true,crossDomain: true})
                  .then(response => {
                    this.success=true;
                    this.error=false;
                    this.timeout();
                
              })
              .catch(error => {
                this.success=false;
                this.error=true;
                  //$(".loading").hide();
               
                //  alert("some thing went wrong...");
              })
        },
        timeout(){
          let that=this;
          setTimeout(function () { that.success=false}, 7000)
        },
        getSettings(){
          this.ajaxSetup();
         $.post(auth_url+"/v1/getUserSetting",
                     {},{withCredentials: true,crossDomain: true})
                     .then(response => {
                     //  console.log(response);
                     if(response.message!=null)
                       this.userSetting=response.message;
                 })
                 .catch(error => {
                     //$(".loading").hide();
                  
                   //  alert("some thing went wrong...");
                 })
           }
   
    }})

