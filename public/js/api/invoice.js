// ------------------------------------ Vue Object  Start -------------------
const months = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December'
}
const app = new Vue({
  el: '#invoice-app',

  data: {
    enablebutton: false,

    sortKey: 'name',

    reverse: false,

    search: '',

    columns: ['Invoice Number', 'Invoice Date', 'Period', 'no trees', 'amount', 'status'],

    newUser: {},

    invoices: []
  },
  filters: {
    uppercase: function (value) {
      return value.toUpperCase() || "";
    },
    formatDate: function(value){
      const d = new Date(value)
      const year = d.getFullYear() // 2019
      const date = d.getDate() // 23
      const monthName = months[d.getMonth()]
      const formatted = `${date} ${monthName} ${year}`;
      return formatted;
    }
  },

  methods: {
    sortBy: function (sortKey) {
      this.reverse = (this.sortKey == sortKey) ? !this.reverse : false;

      this.sortKey = sortKey;
    },
    ajaxSetup() {
      $.ajaxSetup({
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", document.cookie);
        }
      });
    },
    getInvoices() {
      let that = this;
      this.ajaxSetup();

      $.get("/invoice/list", { withCredentials: true, crossDomain: true })
        .then(response => {
          console.log(response);
          that.invoices = response.data
        })
        .catch(error => {
          //$(".loading").hide();
          $(".loader").css({ "display": "none" });
           alert("some thing went wrong...");
        })
    },
    saveSubscription(data) {
      console.log("Save Subscription called");
      this.ajaxSetup();
      $.post("/save-subscription", data, { withCredentials: true, crossDomain: true })
        .then(response => {
          console.log(response);
          window.location.reload(true);
        })
        .catch(error => {
          //$(".loading").hide();
          $(".loader").css({ "display": "none" });
           alert("something went wrong...");
        })
    },
    cancelSubscription(data) {
      console.log("Cancel Subscription called");
      this.ajaxSetup();
      $.post("/cancel-subscription", data, { withCredentials: true, crossDomain: true })
        .then(response => {
          console.log(response);
          alert("Sucessfully cancelled");
          window.location.reload(true);
        })
        .catch(error => {
          //$(".loading").hide();
          $(".loader").css({ "display": "none" });
           alert("something went wrong...");
        })
    },
  }
});

app.getInvoices();
//
function cancel(){
    app.cancelSubscription({subscription})
}


// -------- Paypal Button Config
if(!subscription){
paypal.Buttons({
  style: {
    size: 'responsive',
    shape : 'pill',
    color : 'blue',
    layout: 'horizontal'
},
  createSubscription: function(data, actions) {
  
    return actions.subscription.create({
  
      // 'plan_id': 'P-85A69115LA822772MLXFK4WQ', // First
      // 'plan_id': 'P-51J0712717610912VLXPEWDY', // new
      // 'plan_id': 'P-0AA777765H8733147LXPZ2CI', // newest 28 Nov
      // 'plan_id': 'P-50D70359HC124441ULXP2OLI', // newest 28 Nov 3pm
      // 'plan_id': 'P-5HT47816C3166133ELXP2ZKI', // newest 28 Nov 4pm
      // 'plan_id': 'P-06D957388H9129332LXP5QAY', // newest 28 Nov 7pm
      'plan_id': planId, // newest 11 Dec 12pm
      'quantity':1,
      // 'auto_renewal':true, 
    });
  
  },
  onApprove: function(data, actions) {
    console.log(data);
    alert('You have successfully created subscription ' + data.subscriptionID);
    app.saveSubscription(data)
  },
  onCancel: function (data) {
    alert("Subscription Registration cancelled");
    // Show a cancel page, or return to cart
  },
  onError: function (err) {
    // Show an error page here, when an error occurs
    alert(err);
  }
  
  
  }).render('#paypal-button-container');
}
