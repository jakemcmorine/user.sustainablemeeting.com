const appHeader = new Vue({
    el: '#headerVue',
    data: {
      datas: [],
      treeCount:"",
      isUser:true,
      meetingCount:"",
      treeplanted:"",
      userSetting: {
       },
      id:"",
      success:false,
    },
    
   
    methods: {
    
      ajaxSetup(){
          $.ajaxSetup({
            beforeSend: function (xhr)
            {
               xhr.setRequestHeader("Authorization",document.cookie);        
            }
        });
      },
     changeTreeCount(){
           if(this.isUser==true){
               this.isUser=false;
               meetingUrlHeader='/meetings/count'
               this.getMeetingCounts();
           }else{
                this.isUser=true;
                meetingUrlHeader='/meetings/countUser'
                this.getMeetingCounts();
           }
     },
      getMeetingCounts(){
       this.ajaxSetup();
      $.post(meetingUrlHeader,
                  {},{withCredentials: true,crossDomain: true})
                  .then(response => {
                      console.log(response)
                      this.meetingCount=response.meetingCount;
                      this.treeplanted=response.meetingInviteCount[0].meeting_invites_Count;
                    this.success=true;
                   // this.timeout();
              })
              .catch(error => {
                  //$(".loading").hide();
               
                //  alert("some thing went wrong...");
              })
        },
      
   
    }})

    appHeader.getMeetingCounts();
