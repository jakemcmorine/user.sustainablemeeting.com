//logout
function logout(){
    $.ajaxSetup({
        beforeSend: function (xhr)
        {
           xhr.setRequestHeader("Authorization",document.cookie);        
        }
    });
    $.post(auth_url+"/v1/logout",
        {},{withCredentials: true,crossDomain: true})
        .then(response => {
            deleteAllCookies()
            window.location="/";
  
    })
    .catch(error => {
        //$(".loading").hide();
        deleteAllCookies();
       window.location="/"
    //  alert("some thing went wrong...");
    })
}
function deleteAllCookies() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++)
      eraseCookie(cookies[i].split("=")[0]);
}
function eraseCookie(name) {
    createCookie(name,"",-1);
}
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}
let cookie=document.cookie;
let jwt=cookie.split("token=")
if(jwt[1]==null||jwt[1]==''||jwt[1]==undefined){
    window.location="/";
}
//console.log('cookie',cookie)

