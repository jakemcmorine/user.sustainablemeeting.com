
const app = new Vue({
  el: '#dashboardview',
  data: {
    datas: [],
    treeCount: "",
    meetingCount: "",
    treeplanted: "",
    userSetting: {
    },
    id: "",
    success: false,
    chartScale: 'monthly',
    chartType: 'meetings'
  },

  methods: {
    getAll() {

    },
    deleteUSer(id) {
      this.id = id;
    },
    ajaxSetup() {
      $.ajaxSetup({
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", document.cookie);
        }
      });
    },
    handleSubmitEmailSceduler(e) {
      //alert(e)

    },
    handleSubmiDomain(e) {
      //  alert(e)

    },
    handleSubmiInfoUrl(e) {

    },
    handleSubmitFooter(e) {

    },
    shoLoader() {
      $(".loader").css({ "display": "none" });
      var frm = document.getElementsByName('add-manually')[0];
      $("#scss").text("user deleted successfully")
      $("#error").hide();
    },
    hideLoader() {
      $(".loader").css({ "display": "none" });
      $("#success").hide();
      $("#error").show();
    },
    getMeetingCounts() {
      this.ajaxSetup();
      $.post(meetingUrl,
        {}, { withCredentials: true, crossDomain: true })
        .then(response => {
          console.log(response)
          this.meetingCount = response.meetingCount;
          this.treeplanted = response.meetingInviteCount[0].meeting_invites_Count;
          this.success = true;
          this.timeout();
        })
        .catch(error => {
          //$(".loading").hide();

          //  alert("some thing went wrong...");
        })
    },
    timeout() {
      let that = this;
      setTimeout(function () { that.success = false }, 7000)
    },
    getSettings() {
      this.ajaxSetup();
      $.post(auth_url + "/v1/getUserSetting",
        {}, { withCredentials: true, crossDomain: true })
        .then(response => {
          //  console.log(response);
          if (response.message != null)
            this.userSetting = response.message;
        })
        .catch(error => {
          //$(".loading").hide();

          //  alert("some thing went wrong...");
        })
    },
    resetChart() {
      axios.post(`/chartdata/admin`,
        { type: this.chartType, scale: this.chartScale },
        { withCredentials: true, crossDomain: true })
        .then(response => {
          if (!response.data && !response.data.data) return false;
          let data = response.data.data[0];
          console.log(data);

          if (this.chartScale === 'yearly') {
            this.generateYearGraph(data);
          } else if (this.chartScale === 'monthly') {
            this.generateMonthGraph(data);
          } else if (this.chartScale === 'weekly') {
            this.generateWeekGraph(data);
          } else if (this.chartScale === 'daily') {
            this.generateDayGraph(data);
          }
        })
        .catch(error => {

        });
    },

    reloadChart(labels, data, labelString) {
      const ctx = document.getElementById('myChart');
      const myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
            label: `# of ${this.chartType === 'meetings' ? 'Meetings' : 'Trees Planted'}`,
            data: data,
            fill: false,
            borderColor: 'rgb(103, 146, 102)', // Add custom color border (Line)
            backgroundColor: 'rgb(103, 146, 102)', // Add custom color background (Points and Fill)
          }]
        },
        options: {
          legend: {
            display: false
          },
          responsive: true,
          hover: {
            mode: 'nearest',
            intersect: true
          },
          scales: {
            xAxes: [{
              display: true,
              scaleLabel: {
                display: true,
                labelString
              },
              gridLines: {
                display:false
              } 
            }],
            yAxes: [{
              display: true,
              scaleLabel: {
                display: true,
                labelString: `${this.chartType === 'meetings' ? 'Meetings' : 'Trees Planted'}`
              },
              ticks: {
                beginAtZero: true,
                stepSize: 1,
                suggestedMax: 5
              },
              gridLines: {
                display:false
              } 
            }]
          }
        }
      });
    },
    generateYearGraph(data) {
      let labels = [];
      let rd = [];
      let y = moment().year();
      for (let i = y - 4; i <= y; i++) {
        labels.push(i);
      }
      labels.map(label => {
        let flag = true;
        data.map(x => {
          if (x.frequency === label) {
            rd.push(x.item);
            flag = false;
          }
        });
        if (flag) {
          rd.push(0);
        }
      });
      this.reloadChart(labels, rd, 'Year');
    },
    generateMonthGraph(data) {
      let month = [];
      let labels = [];
      let rd = [];
      for (let i = -12; i <= 0; i++) {
        month.push(moment().add(i, 'months'));
      }
      month.map(element => {
        let flag = true;
        data.map(x => {
          if (moment(element).isSame(x.frequency, 'month')) {
            rd.push(x.item);
            flag = false;
          }
        });
        if (flag) {
          rd.push(0);
        }
        labels.push(moment(element).format('MMM YYYY'));
      })
      this.reloadChart(labels, rd, 'Month');
    },
    generateWeekGraph(data) {
      let w = moment().week();
      let labels = [];
      rd = [];
      for (let x = 0; x < 12; x++) {
        labels.push(w - x);
      }
      labels.reverse();
      labels.map(label => {
        let flag = true;
        data.map(x => {
          if (x.frequency === label) {
            rd.push(x.item);
            flag = false;
          }
        });
        if (flag) {
          rd.push(0);
        }
      });
      this.reloadChart(labels, rd, 'Week');
    },
    generateDayGraph(data) {
      let month = [];
      let labels = [];
      let rd = [];
      for (let i = -35; i <= 0; i++) {
        month.push(moment().add(i, 'day'));
      }
      month.map(element => {
        let flag = true;
        data.map(x => {
          if (moment(element).isSame(x.frequency, 'day')) {
            rd.push(x.item);
            flag = false;
          }
        });
        if (flag) {
          rd.push(0);
        }
        labels.push(moment(element).format('DD MMM YYYY'));
      })
      this.reloadChart(labels, rd, 'Date');
    }
  }
})

app.getMeetingCounts();
app.resetChart();
if (meetingUrl == '/meetings/count') app.getSettings();
