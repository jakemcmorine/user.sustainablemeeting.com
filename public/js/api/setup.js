Vue.use(VeeValidate);
const app = new Vue({
    el: '#setup12',
    data: {
      datas: [],
      userSetting: {
       },
      id:"",
      success:false,
    },
    
   
    methods: {
      getAll() {
       
      },
      deleteUSer(id){
          this.id=id;
      },
      ajaxSetup(){
          $.ajaxSetup({
            beforeSend: function (xhr)
            {
               xhr.setRequestHeader("Authorization",document.cookie);        
            }
        });
      },
      handleSubmitEmailSceduler(e) {
          //alert(e)
        this.submitted = true;
        this.$validator.validateAll("step1").then(valid => {
            if (valid) {
               this.updateSettings({"emailSend":this.userSetting.emailSend,"hour":this.userSetting.hour,"minute":this.userSetting.minute})
            }else{
                console.log(this.$validator)
               // alert("false")
            }
        });
    },
    handleSubmiDomain(e) {
      //  alert(e)
      this.submitted = true;
      this.$validator.validateAll("step2").then(valid => {
          if (valid) {
            this.updateSettings({"domain_url":this.userSetting.domain_url})
          }else{
             // alert("false")
          }
      });
  },
    handleSubmiInfoUrl(e) {
        alert(e)
      this.submitted = true;
      this.$validator.validateAll("step3").then(valid => {
          if (valid) {
            this.updateSettings({"info_url":this.userSetting.info_url})
          }else{
             // alert("false")
          }
      });
  },
  handleSubmitFooter(e) {
    var desc = CKEDITOR.instances.footerr.getData();
    this.userSetting.footer=desc;
    this.submitted = true;
   // alert(desc)
    this.$validator.validateAll("step4").then(valid => {
        if (valid) {
            this.updateSettings({"footer":this.userSetting.footer})
         }else{
           console.log(this.$validator)
           // alert("false")
         }
      });
    },
      shoLoader(){
         $(".loader").css({"display":"none"});
         var frm = document.getElementsByName('add-manually')[0];
         $("#scss").text("user deleted successfully")
         $("#error").hide();
      },
      hideLoader(){   
          $(".loader").css({"display":"none"});
          $("#success").hide();
          $("#error").show();
      },
      updateSettings(data){
       this.ajaxSetup();
      $.post(auth_url+"/v1/updateUserSetting",
                  data,{withCredentials: true,crossDomain: true})
                  .then(response => {
                    this.success=true;
                    this.timeout();
                
              })
              .catch(error => {
                  //$(".loading").hide();
               
                //  alert("some thing went wrong...");
              })
        },
        timeout(){
          let that=this;
          setTimeout(function () { that.success=false}, 7000)
        },
        getSettings(){
          this.ajaxSetup();
         $.post(auth_url+"/v1/getUserSetting",
                     {},{withCredentials: true,crossDomain: true})
                     .then(response => {
                     //  console.log(response);
                     if(response.message!=null)
                       this.userSetting=response.message;
                 })
                 .catch(error => {
                     //$(".loading").hide();
                  
                   //  alert("some thing went wrong...");
                 })
           }
   
    }})

    app.getSettings();