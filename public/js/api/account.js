var json;
var myJSON;
Vue.use(VeeValidate);

var inviteformstrings;
    const app = new Vue({
      el: '#accountDet',
      data: {
        psdChange:{},
        password: "",
        submit:false,
        email:"",
        emailSuccmsg:"",
        emailErrorMsg:"",
        isEmailValid:false,
        submitPSD:false,
        confirmpassword: "",
        userdet:{
          chnge:{},
          saved:{}
        },
        isChangeMail:false,
        id:"",
        changPsd:false,
        success:false,
        error:false,
        successEmail:false,
        errorEmail:false,
        sccsmsg:"",
        errormsg:"",
        sccsmsgEmail:"",
        errormsgEmail:"",
        successDet:false,
        errorDet:false,
        sccsmsgDet:"",
        errormsgDet:""
      },
      methods: {
        ajaxSetup(){
            $.ajaxSetup({
              beforeSend: function (xhr)
              {
                 xhr.setRequestHeader("Authorization",document.cookie);        
              }
          });
        },
        changEmail(){
           if(this.isChangeMail){
             this.isChangeMail=false;
           }else{
              this.isChangeMail=true;
           }
        },
        emailChangeSubmit(){
          this.submitPSD=true;
            this.$validator.validateAll("emailchange").then(valid => {
                if (valid) {
                    this.changeEmail()
                 }else{
                   console.log(this.$validator)
                   // alert("false")
                 }
              });
        },
        changPsdShow(){
           if(this.changPsd){
            this.changPsd=false;
           }else{
            this.changPsd=true;
           }
        },
        psdChangeSubmit(){
          this.submitPSD=true;
            this.$validator.validateAll("changePsd").then(valid => {
                if (valid) {
                    this.changePsd()
                 }else{
                   console.log(this.$validator)
                   // alert("false")
                 }
              });
        },
        saveDetailsSubmit(){
          this.submit=true;
          this.$validator.validateAll("acntDetail").then(valid => {
              if (valid) {
                  this.saveDetail()
               }else{
                 console.log(this.$validator)
                 // alert("false")
               }
            });
      },
      saveDetail(){
        let that=this;
     this.ajaxSetup();
     
    $.post(auth_url+"/v1/saveDetail",
                this.userdet.chnge,{withCredentials: true,crossDomain: true})
                .then(response => {
                    $(".loader").css({"display":"none"});
                    that.successDet=true;
                    that.errorDet=false;
                    that.userdet.saved=that.userdet.chnge
                    that.sccsmsgDet=response.message;
           })
            .catch(error => {
                //$(".loading").hide();
                $(".loader").css({"display":"none"});
                that.errorDet=true;
                that.successDet=false;
                that.errormsgDet=error.responseJSON.error;
            
              //  alert("some thing went wrong...");
            })
      },
    changePsd(){
        let that=this;
     this.ajaxSetup();
     var pathname = window.location.pathname
     pathname=pathname.split("/")
     console.log(pathname);
    $.post(auth_url+"/v1/changepassword",
                this.psdChange,{withCredentials: true,crossDomain: true})
                .then(response => {
                    $(".loader").css({"display":"none"});
                    that.success=true;
                    that.error=false;
                    that.sccsmsg=response.message;
                    var frm = document.getElementsByName('add-manually')[0];
                   $("#scss").text("user deleted successfully")
                    $("#error").hide();
            })
            .catch(error => {
                //$(".loading").hide();
                $(".loader").css({"display":"none"});
                that.error=true;
                that.success=false;
                that.errormsg=error.responseJSON.error;
                            //  alert("some thing went wrong...");
            })
      },
      changeEmail(){
        let that=this;
        this.ajaxSetup();
        var pathname = window.location.pathname
        pathname=pathname.split("/")
        console.log(pathname);
        $.post(auth_url+"/v1/emailChange",
                    {email:this.email},{withCredentials: true,crossDomain: true})
                    .then(response => {
                        $(".loader").css({"display":"none"});
                        that.successEmail=true;
                        that.errorEmail=false;
                        that.sccsmsgEmail=response.message;
                        var frm = document.getElementsByName('add-manually')[0];
                      $("#scss").text("user deleted successfully")
                        $("#error").hide();
                })
                .catch(error => {
                    //$(".loading").hide();
                    $(".loader").css({"display":"none"});
                    that.errorEmail=true;
                    that.successEmail=false;
                    that.errormsgEmail=error.responseJSON.error;
                                //  alert("some thing went wrong...");
                })
          },
 getProfileData(){
      let that=this;
     this.ajaxSetup();
     $.post(auth_url+"/v1/getAccountDet",
                {},{withCredentials: true,crossDomain: true})
                .then(response => {
                    $(".loader").css({"display":"none"});
                    that.userdet.chnge=response.message;
                    that.userdet.saved=response.message;
                    that.email=response.message.email;
            })
            .catch(error => {
                //$(".loading").hide();
                $(".loader").css({"display":"none"});
                that.errormsg=error.responseJSON.error;
                            //  alert("some thing went wrong...");
            })
      },
      emailVarify(tocken){
        let that=this;
       this.ajaxSetup();
       $.get(auth_url+"/v1/email/verify/"+tocken,
                  {},{withCredentials: true,crossDomain: true})
                  .then(response => {
                      $(".loader").css({"display":"none"});
                      that.successDet=true;
                      that.sccsmsgDet=response.message;
                      setTimeout(function(){ logout(); }, 2000);
                      // that.userdet.saved=response.message
                    
              })
              .catch(error => {
                  //$(".loading").hide();
                  $(".loader").css({"display":"none"});
                  that.errorDet=true;
                  that.errormsgDet=error.responseJSON.error;
                 // setTimeout(function(){ logout(); }, 2000);
                 setTimeout(function(){ window.location.href='/accountdetails'; }, 1000);
                              //  alert("some thing went wrong...");
              })
        },
 
   },
  
   });
let currentUrl=window.location.href;
currentUrl=currentUrl.split('varify/')
console.log(currentUrl)
if(currentUrl[1]){
   app.emailVarify(currentUrl[1])
}
   app.getProfileData();
   

