jQuery(document).ready(function() {
    jQuery('#msgDiv').hide();
    jQuery('#overlay').hide();

    jQuery('#frmLogin').validate({
        rules: {
            email: {
                required:true,
                email:true
            },
            password: {
                required:true
            }
        },
        messages: {
            email: {
                required: "Please Enter Email",
                email: "Please Enter valid Email"
            },
            password:  {
                required:"Please Enter Password"
            }
        },
        submitHandler: function() {
            jQuery('#btnSubmit').attr('disabled','disabled');
            jQuery('#btnSubmit').text('Wait..');
            // jQuery('#overlay').show();
            //openOverlay();
            var formData = jQuery('#frmLogin').serializeArray();
            console.log("formData",formData);
            jQuery.ajax({

                url: '/userSignUp',
                data: formData,
                type: 'post',
                success: function (result) 
                {
                    console.log("success result",result)
                    if(result.status === 200)
                    {
                        window.location.href = '/dashboard';
                    }
                    else if(result.status === 401){
                        jQuery('#msgDiv').show();
                        jQuery('#msgDiv').html(result.message);
                    }
                },
                error:function(result){
                    console.log("error result",result)
                    jQuery('#msgDiv').html(result);
                }
            })
        }
    });
});