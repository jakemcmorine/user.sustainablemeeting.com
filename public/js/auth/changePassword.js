jQuery(document).ready(function() {
    jQuery.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
    });

    if(bNoIssue) {
        jQuery('#msgDiv').hide();
    } else{
        jQuery('#errMsg').text('This is an invalid request or token, please verify again.');
        jQuery('#msgDiv').show();
    }

    jQuery('#frmChangePassword').validate({
        rules: {
            password: {
                required:true,
                minlength:6,
                maxlength:12,
                pwcheck:true
            },
            confirmPassword: {
                required:true,
                minlength:6,
                maxlength:12,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "Please Enter Password",
                pwcheck: "Password must includes alphabets and numbers"
            },
            confirmPassword: {
                required: "Please Confirm Your Password",
                equalTo:"The Password should be match"
            }
        },
        submitHandler: function() {
            jQuery('#btnSubmit').attr('disabled','disabled');
            jQuery('#btnSubmit').text('Wait..');
            openOverlay();
            jQuery('#email').val(userEmail);
            jQuery('#actionType').val(actionType);
            var formData = jQuery('#frmChangePassword').serializeArray();
            jQuery.post('/changePassword', formData, function(data) {
                if(data.success === 1){
                    closeOverlay();
                    jQuery('#errMsg').text(data.message);
                    jQuery('#msgDiv').removeClass('alert-danger').addClass('alert-success');
                    jQuery('#msgDiv').show();
                    setTimeout(function() {
                        window.location.href = '/';
                    }, 6000);
                }else {
                    jQuery('#btnSubmit').text('Change Password');
                    jQuery('#btnSubmit').removeAttr('disabled');
                    jQuery('#errMsg').text(data.message);
                    jQuery('#msgDiv').show();
                    closeOverlay();
                }
            });
        }
    });
});