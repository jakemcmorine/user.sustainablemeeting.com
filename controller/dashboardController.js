const axios = require('axios');
const meetingRepository = require("../repositories/meetingRepository");
const Sequelize=require("sequelize");
const moment = require(`moment`);
module.exports.getDashboard = async (req, res) => {
    
    let adminList = [];
    let responseData = await axios.get(`${CONFIG.AUTH_URL}getadmins`);
                
                responseData.data.data.forEach(function(adminDetails){
                    adminList.push(adminDetails)
                })
    res.render('dashboard/dashboard.ejs', { adminList: adminList });
};

module.exports.addAdmins = async (req, res) => {
    let userLogged = {
        "photo": "",
        "firstName": "ash",
        "email": "ash@yopmail.com"
    }
    res.render('admin/addAdmin.ejs', { userLogged: userLogged });
};

module.exports.createAdmin = async (req, res) => {
    try {
        console.log(req.body);
        let activeStatus = (req.body.status == "Active" ? 1 : 0);
        axios.post(`${CONFIG.AUTH_URL}userSignUp`, {
            "name": req.body.name,
            "password": "Ash@7989",
            "email": req.body.email,
            "userRole": "Admin",
            "phonenumber": req.body.phone_number,
            "address": "",
            "added_by": "",
            "contractor_id": -0,
            "pqms_id": -0,
            "is_active": activeStatus,
            "is_deleted": 0,
            "is_confirmed": 0
        })
            .then(function (response) {
                console.log("data",response.data)
                if(response && response.data)
                {
                    //res.redirect("/dashboard")
                    res.send({"status":200,"message":"user Created Successfully","data":response.data})
                }
            })
            .catch(function (error) {
                console.log("error", error)
                res.send({"status":401,"message":"user Creation Error","data":""})
            })
    } catch (err) {
        console.log(err);
        throw err;
    }
};

module.exports.chartDataAdmin = async (req, res) => {
    let b  = req.body;
    let qu = '';
    if (b.type === 'meetings') {
        if (b.scale === 'yearly') {
            qu = `SELECT COUNT(*) item, YEAR(createdAt) frequency FROM meetings WHERE createdAt >= date_sub(now(), interval 5 YEAR) AND org_id = ${req.orgId} GROUP BY YEAR(createdAt)`
        } else if (b.scale === 'monthly') {
            qu = `SELECT COUNT(*) as item, createdAt frequency FROM meetings WHERE createdAt >= date_sub(now(), interval 12 month) AND org_id = ${req.orgId} GROUP BY DATE_FORMAT(createdAt, '%M - %Y')`;
        } else if (b.scale === 'weekly') {
            qu = `SELECT COUNT(*) as item, WEEK(createdAt) frequency FROM meetings WHERE createdAt >= date_sub(now(), INTERVAL 12 WEEK) AND org_id = ${req.orgId} GROUP BY WEEK(createdAt)`;
        } else {
            qu = `SELECT COUNT(*) as item, createdAt frequency FROM meetings WHERE createdAt >= date_sub(now(), INTERVAL 35 DAY) AND org_id = ${req.orgId} GROUP BY DAY(createdAt)`;
        }
    } else if (b.type === `treePLanted`) {
        qu = findTreePlantedCount(req);
    }
    let meetingsForAdmin = await meetingRepository.getMeetingsForAdmin(qu);
    res.send({"status":200,"message":"user Created Successfully","data":meetingsForAdmin});
}


const findTreePlantedCount = req => {
    let b =  req.body;
    let qu = '';
    if (b.scale === 'yearly') {
        qu = `SELECT COUNT(m.id) item, YEAR(mi.emailsent_date) frequency FROM meetings m INNER JOIN meeting_invites mi on m.id = mi.meeting_id WHERE mi.emailsent_date >= date_sub(now(), interval 5 YEAR) AND org_id = ${req.orgId} GROUP BY YEAR(mi.emailsent_date)`
    } else if (b.scale === 'monthly') {
        qu = `SELECT COUNT(m.id) item, mi.emailsent_date frequency FROM meetings m INNER JOIN meeting_invites mi on m.id = mi.meeting_id WHERE mi.emailsent_date >= date_sub(now(), interval 12 MONTH) AND org_id = ${req.orgId} GROUP BY DATE_FORMAT(mi.emailsent_date, '%M - %Y')`;
    } else if (b.scale === 'weekly') {
        qu = `SELECT COUNT(m.id) item, WEEK(mi.emailsent_date) frequency FROM meetings m INNER JOIN meeting_invites mi on m.id = mi.meeting_id WHERE mi.emailsent_date >= date_sub(now(), interval 12 WEEK) AND org_id = ${req.orgId} GROUP BY WEEK(mi.emailsent_date)`;
    } else {
        qu = `SELECT COUNT(m.id) item, mi.emailsent_date frequency FROM meetings m INNER JOIN meeting_invites mi on m.id = mi.meeting_id WHERE mi.emailsent_date >= date_sub(now(), interval 35 DAY) AND org_id = ${req.orgId} GROUP BY DAY(mi.emailsent_date)`;
    }
    return qu;
}