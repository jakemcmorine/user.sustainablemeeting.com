
const {findOrgSubscription, saveSubscription, updateStatus}=require("../repositories/subscriptionRepository");
const {list }=require("../repositories/invoiceRepository");
const {cancelPaypalSubscription }=require("../services/apiCall");
module.exports.history = async(req,res)=>{
    try {
        let subscription = await findOrgSubscription(req.orgId);
        let enableButton = (req.orgId === req.userId)
        console.log(subscription, "****\n", enableButton);
        let planId   = CONFIG.paypalPlanId;
        let clientId = CONFIG.paypalClientId;
        console.log( planId, clientId)
        res.render('invoiceHistory.ejs', {subscription, enableButton, planId, clientId});
    } catch (error) {
        console.log(error);
        res.render('404.ejs');
    }    
};

module.exports.listInvoices = async(req,res)=>{
    try {
        let invoices = await list(req.orgId);
        return res.status(200).send({"status":200,"message":"Invoice listed Successfully","data":invoices});
    } catch (error) {
        console.log(error);
        res.status(400).send({"status":400,"message":"Some error occured","data":[]});
      }    
};

module.exports.saveSubscription = async(req,res)=>{
    try {
        console.log(req.body);
        let data = req.body;
        data.orgId = req.orgId
        let id = await saveSubscription(data);
        return res.status(200).send({"status":200,"message":"Subscription Saved Successfully"});
    } catch (error) {
        console.log(error);
        res.status(400).send({"status":400,"message":"Some error occured","data":[]});
      }    
};
module.exports.cancelSubscription = async(req,res)=>{
    try {
        console.log(req.body);
        let data = req.body.subscription;
        let id = await cancelPaypalSubscription(data);
        let staus = await updateStatus(data);
        console.log("cancel", id);
        return res.status(200).send({"status":200,"message":"Subscription Cancled Successfully"});
    } catch (error) {
        console.log(error);
        res.status(400).send({"status":400,"message":"Some error occured","data":[]});
      }    
};