const fs=require("fs")
const certficateModel=require("../models").certificate
const meetingRepository=require("../repositories/meetingRepository")
var moment = require('moment');
var {Op} = require('sequelize');
const puppeteer = require('puppeteer');

var ejs = require('ejs');
var pdf = require('html-pdf')
var uniqid = require('uniqid');
class CertificateRepository {
  //User Sign Up Module
  async createCertificate(id,email) {
    let certfDet=await this.findOneMOdel({where:{meeting_id:id}});
    certfDet=JSON.stringify(certfDet) 
    certfDet=JSON.parse(certfDet)
    if(certfDet){
      let filepath=certfDet.certficate_name;
       if(fs.existsSync('./public/certificates/'+filepath)){
          return certfDet;
       }else{
          let fileName=await this.takeScreenshot(certfDet)
          certfDet['certficate_name']=fileName;
          let crtfct=await this.updateModel({certficate_name:fileName},{where:{meeting_id:id}})
          return certfDet;
       }
    }else{
          let meetingClientdetail=await meetingRepository.getMeetingInvite({where:{meeting_id:id,email:email}});
          let meetingCount=await meetingRepository.getMeetingInvites({where: {email:email,
            emailsent_date: {
              [Op.lte] :moment(meetingClientdetail.dataValues.emailsent_date) 
            }
          }});
          meetingCount=meetingCount.count;
          console.log("length",meetingCount);
          let certificate_details={"meeting_id":id,"number_of_tree_planted":meetingCount,"certificate_date":moment(meetingClientdetail.dataValues.emailsent_date).format("DD-MMMM-YYYY"),"email":email,"name":meetingClientdetail.first_name}
         // let pdfFileName=await this.createPdf(certificate_details)
          let fileName=await this.takeScreenshot(certificate_details)
          certificate_details["certficate_name"]=fileName;
          console.log(fileName);
          await this.createMOdel(certificate_details)
          return certificate_details;
     }
        
  }
 

async  takeScreenshot(data)  {
  const browser = await puppeteer.launch({headless: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const page = await browser.newPage();
  await page.setViewport({ width: 605, height:840});
  let certificateData=JSON.stringify(data);
  await page.goto('http://localhost:3003/certificateScreenshot?data='+certificateData);
  let fileName=uniqid();
  if (!fs.existsSync('./public/certificates')){
     fs.mkdirSync('./public/certificates');
  }
  await page.screenshot({path: './public/certificates/'+fileName+'.jpg',quality:100});
  await browser.close();
  return fileName+'.jpg';
}
  async findOneMOdel(where){
    let certfDet=await certficateModel.findOne(where,{plain:true});
    return certfDet;
  }
  async createMOdel(data){
    let certfcate=await certficateModel.create(data);
    return certfcate;
  }
  async updateModel(value,where){
      try{
        let update=await certficateModel.update(value,where)
        console.log(update);
        return;
      }catch(err){
          console.log(err);
      }
  }
  async createPdf(certificate_details){
    var compiled = ejs.compile(fs.readFileSync(__dirname + '/pdf.html', 'utf8'));
    var html = compiled(certificate_details);
    let fileName=uniqid();
    return new Promise(function(resolve, reject) {
        pdf.create(html).toFile('./public/certificates/'+fileName+'.pdf',(err,result) => {
            console.log(err)
            if(err){
                reject(error)
            }else{
                resolve(result)
            }
        })
      });
      
  }

}

module.exports = new CertificateRepository();