const fs=require("fs")
const invitationModel=require("../models").invitation
const Mail=require("../services/MailService")
class invitationRepository{
    async create(data){
        let invitation=await this.find({where:{email:data.email}}) 
        if(invitation){
            throw "Invitation already received";
        }else{
            let mailt=await invitationModel.create(data);
            await Mail.sendIvitationMail(data)
            return mailt;
        }
    }
    async find(where){
        try{
             let invitation=await invitationModel.findOne(where);
             return invitation;
        }catch(error){
            throw error;
        }
    }
}

module.exports = new invitationRepository();





