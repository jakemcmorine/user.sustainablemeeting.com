const fs = require("fs");
const Sequelize = require("sequelize")
const invoiceModel = require("../models").invoice;
class InvoiceRepository {
  //User Sign Up Module
  async list(id) {
    let details = await invoiceModel.findAll({
      where: {
        orginasation_id: id
      },
      order: [
        ['id', 'DESC']
      ]
    });
    return details;
  }
}

module.exports = new InvoiceRepository();