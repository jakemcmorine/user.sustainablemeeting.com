const fs=require("fs");
const Sequelize=require("sequelize")
const meetingModel=require("../models").meetings;
const meeting_invites_Model=require("../models").meeting_invites;
const model  =require("../models");
class MeetingRepository {
  //User Sign Up Module
  async findMeeting(id) {
     let meetingDet=await meetingModel.findOne({where:{unq_id:id}});
     return meetingDet;
   
  }
  async meetingCount(where) {
    let meetingCount=await meetingModel.count(where);
    console.log(meetingCount)
    return meetingCount;
 }
 async meetingInviteCount(where) {
  let meetingCount=await meetingModel.findAll({where:where,
    //   attributes: { 
    //       include: [[Sequelize.fn("COUNT", Sequelize.col("meeting_invites.meeting_id")), "historyModelCount"]] 
    //   },
   attributes: [
     // [Sequelize.fn('count', Sequelize.col('meetings.id')), 'total_user'],
      [Sequelize.fn('count', Sequelize.col('meeting_invites.meeting_id')), 'meeting_invites_Count']
    ],
      include: [{
        model: meeting_invites_Model, attributes: []
     }],
     
      
  });
  console.log(meetingCount)
  return meetingCount;

}
  async getMeetingInvite(where){
      try{
        let meetingInvite=await meeting_invites_Model.findOne(where);
        console.log(meetingInvite);
        return meetingInvite;
      }
      catch(error){
          console.log(error);
      }
   
  //  return meetingInvite;
  }
  async getMeetingInvites(where){
    let meetingInvites=await meeting_invites_Model.findAndCountAll(where);
    return meetingInvites;
  }
  async getMeetingInfos(where){
    let meetingInfos=await meetingModel.findAll(where);
    // res.json({data:datal});
    return meetingInfos;
  }
  async getMeetingsForAdmin(where){
    let meetingsForAdmin = await meetingModel.sequelize.query(where);
    return meetingsForAdmin;
  }

}

module.exports = new MeetingRepository();