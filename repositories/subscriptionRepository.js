const fs=require("fs");
const Sequelize=require("sequelize")
const SubscriptionModel=require("../models").subscription;
(()=>{
  Date.prototype.nextBilling = function(){
  this.setDate(this.getDate() + 29);
  return this
  }
})();

class SubscriptionRepository {
  //User Sign Up Module
  async findOrgSubscription(id) {
     let details= await SubscriptionModel.findOne({where:{orginasation_id:id, status:'active'}});
     return details;
   
  };

  async saveSubscription(data){
    let item = {
      orginasation_id: data.orgId,
      subscriptionId: data.subscriptionID,
      orderId: data.orderID,
      billingDate: new Date().nextBilling(),
      status:'active'
    }
    let id = await SubscriptionModel.create(item)
    return id;
  }

  async updateStatus(subscriptionId, status = 'cancelled'){
    console.log(subscriptionId, status)
    let res = await SubscriptionModel.update({status},{where: {subscriptionId}})
    return res;
  }

}
module.exports = new SubscriptionRepository();