
const {promisify} = require('util');

class redisModel {
	constructor(Rclient){
		this.getAsync = promisify(Rclient.get).bind(Rclient);
		this.setAsync = promisify(Rclient.set).bind(Rclient);
		this.hmset    = promisify(Rclient.hmset).bind(Rclient);
		this.hgetall  = promisify(Rclient.hgetall).bind(Rclient);
		this.exists   = promisify(Rclient.exists).bind(Rclient);
		this.expire   = promisify(Rclient.expire).bind(Rclient);
		this.hincrby  = promisify(Rclient.hincrby).bind(Rclient);

	}
}

module.exports =  redisModel;
