
const redis = require("redis");

const Rclient = redis.createClient({host :CONFIG.redis_host, port :CONFIG.redis_port});
Rclient.on("error", function (err) {
    console.log("Error conecting to redis server",err);
});
Rclient.on("connect", function (success) {
    console.log("connected to redis server");
});


module.exports = Rclient;
