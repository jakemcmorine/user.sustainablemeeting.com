'use strict';
module.exports = (sequelize, DataTypes) => {
  const meeting_invites = sequelize.define('meeting_invites', {
    meeting_id: DataTypes.STRING,
    email: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    title: DataTypes.STRING,
    emailsent_date:DataTypes.DATE,
    status: DataTypes.STRING
  }, {});
  meeting_invites.associate = function(models) {
    meeting_invites.belongsTo(models.meetings,{foreignKey: 'meeting_id' })
    // associations can be defined here
   // meeting_invites.belongsTo(models.meetings)
  };
  return meeting_invites;
};