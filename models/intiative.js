'use strict';
module.exports = (sequelize, DataTypes) => {
  const Intiative = sequelize.define('Intiative', {
    initiative_name: DataTypes.STRING,
    ini_certificate_name: DataTypes.STRING,
    local_name: DataTypes.STRING,
    website: DataTypes.STRING,
    sender_email_title: DataTypes.STRING,
    sender_certificate_email_title: DataTypes.STRING,
    request_for_funds: DataTypes.STRING,
    activation_email: DataTypes.STRING,
    ini_counter_type: DataTypes.STRING,
    cert_type: DataTypes.STRING,
    social_text: DataTypes.STRING,
    company_text: DataTypes.STRING,
    user_text: DataTypes.STRING,
    certificate1: DataTypes.STRING,
    certificate2: DataTypes.STRING,
    certificate3: DataTypes.STRING,
    certificate4: DataTypes.STRING,
    certificate_layout: DataTypes.STRING,
    bk_image: DataTypes.STRING,
    user_id: DataTypes.NUMBER
  }, {});
  Intiative.associate = function(models) {
    // associations can be defined here
  };
  return Intiative;
};