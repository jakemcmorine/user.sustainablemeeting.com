'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_clients = sequelize.define('user_clients', {
    user_id: DataTypes.NUMBER,
    client_id: DataTypes.NUMBER
  }, {});
  user_clients.associate = function(models) {
    // associations can be defined here
  };
  return user_clients;
};