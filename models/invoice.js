'use strict';
module.exports = (sequelize, DataTypes) => {
  const invoice = sequelize.define('invoice', {
    orginasation_id: DataTypes.INTEGER,
    invoiceNumber: DataTypes.STRING,
    invoiceDate: DataTypes.DATE,
    periodStartDate: DataTypes.DATE,
    periodEndDate: DataTypes.DATE,
    numberOfTrees: DataTypes.INTEGER,
    amount: DataTypes.DOUBLE,
    status: DataTypes.ENUM('paid', 'pending', 'canceled'),
  }, {});
  invoice.associate = function(models) {
    // associations can be defined here
  };
  return invoice;
};