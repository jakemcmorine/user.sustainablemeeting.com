'use strict';
module.exports = (sequelize, DataTypes) => {
  const orderItem = sequelize.define('orderItem', {
    invoiceId: DataTypes.INTEGER,
    product:DataTypes.STRING,
    quantity: DataTypes.INTEGER,
  }, {});
  orderItem.associate = function(models) {
    // associations can be defined here
  };
  return orderItem;
};