'use strict';
module.exports = (sequelize, DataTypes) => {
  const meeting_client = sequelize.define('meeting_client', {
    meeting_id: DataTypes.NUMBER,
    client_id: DataTypes.NUMBER,
    status: DataTypes.STRING,
    mail_send: DataTypes.STRING
  }, {});
  meeting_client.associate = function(models) {
    // associations can be defined here
  };
  return meeting_client;
};