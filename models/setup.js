'use strict';
module.exports = (sequelize, DataTypes) => {
  const setup = sequelize.define('setup', {
    sendwhen: DataTypes.STRING,
    hours: DataTypes.INTEGER,
    minutes: DataTypes.INTEGER,
    changedomain: DataTypes.STRING,
    changeurl: DataTypes.STRING,
    invitefooter: DataTypes.STRING
  }, {});
  setup.associate = function(models) {
    // associations can be defined here
  };
  return setup;
};