'use strict';
module.exports = (sequelize, DataTypes) => {
  const clients = sequelize.define('clients', {
    first_name: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: DataTypes.STRING,
    status: DataTypes.STRING
  }, {});
  clients.associate = function(models) {
    // associations can be defined here
  };
  return clients;
};