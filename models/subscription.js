'use strict';
module.exports = (sequelize, DataTypes) => {
  const subscription = sequelize.define('subscription', {
    orginasation_id: DataTypes.INTEGER,
    subscriptionId: DataTypes.STRING,
    orderId: DataTypes.STRING,
    billingDate: DataTypes.DATE,
    status: DataTypes.ENUM('active', 'pending', 'canceled'),
  }, {});
  subscription.associate = function(models) {
    // associations can be defined here
  };
  return subscription;
};