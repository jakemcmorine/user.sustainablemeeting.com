const dotenv = require('dotenv');//instatiate environment variables

if(process.env.NODE_ENV === 'local') {
    dotenv.load({ path: '.env-local' });
}else if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'dev') {
    dotenv.load({ path: '.env-development' });
}
// else if (process.env.NODE_ENV === 'uat' || process.env.NODE_ENV === 'UAT') {
//     dotenv.load({ path: '.env-development-local' });
// }else if(process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'poduction') {
//     dotenv.load({ path: '.env-development-local' });
// }


CONFIG = {};

CONFIG.app_env        = process.env.APP_ENV;
CONFIG.port           = process.env.PORT;
CONFIG.api_host       = process.env.API_OBJECT;
CONFIG.api_feed       = process.env.API_FEED;
CONFIG.admin_host     = process.env.ADMIN_HOST;

CONFIG.secret         = process.env.SECRET;
CONFIG.redis_host     = process.env.REDISHOST;
CONFIG.redis_port     = process.env.REDISPORT;
CONFIG.api_object     = process.env.API_OBJECT

CONFIG.mysql_host     = process.env.MYSQL_HOST;
CONFIG.mysql_port     = process.env.MYSQL_PORT;
CONFIG.mysql_user     = process.env.MYSQL_USER;
CONFIG.mysql_password = process.env.MYSQL_PASSWORD;
CONFIG.mysql_database = process.env.MYSQL_DATABASE;
CONFIG.AUTH_URL       = process.env.AUTH_URL;

CONFIG.AWS_ACCESSKEY  =  process.env.AWS_ACCESSKEY
CONFIG.AWS_ACCESSKEYID = process.env.AWS_ACCESSKEYID
CONFIG.AWS_REGION     = process.env.AWS_REGION  
CONFIG.USER_POOL_ID   = process.env.USER_POOL_ID   
CONFIG.APP_CLIENT_ID  = process.env.APP_CLIENT_ID 

CONFIG.jwt_encryption  = process.env.JWT_ENCRYPTION
CONFIG.username     =process.env.username;
CONFIG.password  = process.env.password;
CONFIG.database  = process.env.database;
CONFIG.host     =process.env.host;
CONFIG.dialect  = process.env.dialect;
CONFIG.operatorsAliases  = process.env.operatorsAliases;

CONFIG.paypalPlanId = process.env.PAYPAL_PLAN
CONFIG.paypalClientId = process.env.PAYPAL_CLIENT
CONFIG.paypalUser = process.env.PAYPAL_USER
CONFIG.paypalPass = process.env.PAYPAL_PASS
CONFIG.paypalUrl = process.env.PAYPAL_URL
module.exports = CONFIG;
