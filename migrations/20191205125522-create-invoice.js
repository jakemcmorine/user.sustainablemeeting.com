'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('invoices', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      orginasation_id: {
        type: Sequelize.INTEGER
      }, 
      invoiceNumber: { 
        type: Sequelize.STRING
      },
      invoiceDate: { 
        type: Sequelize.DATE
      },
      periodStartDate: {
        type:Sequelize.DATE
      },
      periodEndDate: {
        type:Sequelize.DATE
      },
      numberOfTrees: {
        type:Sequelize.INTEGER
      },
      amount: {
        type:Sequelize.DOUBLE
      },
      status: {
        type:   sequelize.ENUM,
        values: ['paid', 'pending', 'canceled']
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('invoices');
  }
};