'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('subscriptions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      orginasation_id: {
        type: Sequelize.INTEGER
      }, 
      subscriptionId: { 
        type: Sequelize.STRING
      },
      orderId: { 
        type: Sequelize.STRING
      },
      billingDate: {
        type:Sequelize.DATE
      },
      status: {
        type:   sequelize.ENUM,
        values: ['active', 'pending', 'canceled']
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('subscriptions');
  }
};