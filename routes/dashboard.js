const express 			= require('express');
const router 			= express.Router();
const dashboardController = require("../controller/dashboardController");
const middleware = require('../middleware/passport');
router.get("/dashboard",middleware.checkjwtToken,dashboardController.getDashboard)
router.get("/addAdmin",middleware.checkjwtToken,dashboardController.addAdmins)
router.post("/addAdmin",middleware.checkjwtToken,dashboardController.createAdmin)
router.post("/chartdata/admin",middleware.checkjwtToken,dashboardController.chartDataAdmin);


module.exports = router;