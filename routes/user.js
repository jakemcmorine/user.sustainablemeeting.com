const express 			= require('express');
const router 			= express.Router();
const userController = require("../controller/userControll")

router.get("/email/varify/:tocken",userController.accountDetails);
router.get("/",userController.userLogin);
router.post("/sentInvitation",userController.sentInvitation);
router.get("/inviteUsers",userController.inviteUsers);
router.get("/accountdetails",userController.accountDetails);
router.get("/userdashboard",userController.userDashboard);
router.get("/forgotPassword",userController.userforgotpassword);
router.get("/usersetup",userController.usersetup);
router.get("/invitationlUsers",userController.invitationlUsers);
router.get("/certificate/download/:img",userController.downloadCertificate); 
router.get("/certificate/:unqId/:email",userController.getCertificate); 
router.get("/certificateScreenshot",userController.generateCertificate);
router.get("/change/psd/:unqId",userController.changePsd); 
router.get("/setNewpassword/:unqId",userController.setPassword); 

// router.get("/invoicehistory",userController.invoicehistory);
router.get("/login",userController.userLogin);
//router.get("/usermeeting",userController.usermeeting);
router.get("/companyinfo",userController.companyinfo);
router.get("/downloadmeeting",userController.downloadmeeting);
router.get("/usersetup",userController.downloadmeeting);




//router.get("/userSignup",userController.userSignup);
//router.post("/userSignUp",userController.postUserSignUp);
//router.get("/verifyUser",userController.verifyUser);
//router.get("/doLogout",userController.logoutUser);
//router.get("/forgotPassword",userController.forgotPassword);

module.exports = router;