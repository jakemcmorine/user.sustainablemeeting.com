const express 			= require('express');
const router 			= express.Router();
const invoiceController = require("../controller/invoiceController")
const middleware = require('../middleware/passport');

router.get("/invoicehistory",middleware.checkjwtToken, invoiceController.history);
router.get("/invoice/list",middleware.checkjwtToken, invoiceController.listInvoices);
router.post("/save-subscription",middleware.checkjwtToken, invoiceController.saveSubscription);
router.post("/cancel-subscription",middleware.checkjwtToken, invoiceController.cancelSubscription);

module.exports = router;