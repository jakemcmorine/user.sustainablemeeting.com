const express 			= require('express');
const router 			= express.Router();
const meetinglists      =require("../controller/meetingController");
const checkAuth         =require("../middleware/passport")
router.get("/list",checkAuth.checkjwtToken,meetinglists.meetingListPage);
router.post("/list",checkAuth.checkjwtToken,meetinglists.meetingList);
router.post("/count",checkAuth.checkjwtToken,meetinglists.meetingCount);
router.post("/countUser",checkAuth.checkjwtToken,meetinglists.meetingCountUser);

router.get("/meetingCount",meetinglists.meetingCount);

module.exports = router;